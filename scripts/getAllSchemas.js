// Obtain all the entity schemas from a farm and store both the original and de drupalized versions.
const fs = require('fs');
const farmOS = require('farmos').default;
const entities = require('../node_modules/farmos/dist/cjs/entities');

// we need to parametrize the farmOS.js object to add more entity types
const { default: defaultEntities, defaultOptions } = entities;
const profile = {
    nomenclature: {
        name: 'profile',
        shortName: 'profile',
        plural: 'profiles',
        shortPlural: 'profiles',
        display: 'Profile',
        displayPlural: 'Profiles',
    },
    defaultOptions,
};

if (!process.env.FARM_DOMAIN) {
    require('dotenv').config({ path:`${__dirname}/../.env` });
};

const aggregatorKey = process.env.FARMOS_KEY;
const farmDomain = process.env.FARM_DOMAIN;

if ( !aggregatorKey || !farmDomain ) {
    let missing = [];
    if (!aggregatorKey) {
        missing.push("FARMOS_KEY");
    };
    if (!farmDomain) {
        missing.push("FARM_DOMAIN");
    };

    throw `One or more fundamental environment variables are missing:  ${missing.join(", ")}. Provide them via an environemt var, either in an .env file or in CI reserved variables.`;
};

const aggregatorKeyAuth = (request, authOpts = {}) => {
    request.interceptors.request.use(
        config => {
            return {
                ...config,
                headers: {
                    ...config.headers,
                    'api-key': aggregatorKey,
                },
                params: {
                    'farm_id': 1,
                },
            };
        },
        Promise.reject,
    );
    return {
        authorize(aggregatorKey) {
        },
    };
};

// Default token functions.
let token;
const getToken = () => token;
const setToken = (t) => { token = t; };

let farm = farmOS(
    {
        remote: {
            host: farmDomain,
            clientId: "farm",
            getToken: getToken,
            setToken: setToken,
            auth: aggregatorKeyAuth
        },
        entities: {
            ... defaultEntities,
            profile
        }
    }
);

console.log(`Retrieving schemas from ${farmDomain}`);

let farmOSjsSchemata = {};

try {
    farmOSjsSchemata = farm.schema.fetch();
    fs.mkdirSync( "reference_collection/", { recursive: true }, console.error );
    farmOSjsSchemata.then( schemata => {
        fs.writeFileSync(`${__dirname}/../reference_collection/farmos.json`, JSON.stringify( schemata.data ) );
    } );
    console.log("Schemata retrieved.");
} catch(e) {
    console.log("error");
    console.log(e);
};

