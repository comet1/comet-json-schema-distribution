const Bar = require('../validators/validate-cjs-example.js');

const barPass = {
    bar: "something"
};

const barFail = {
    // bar: "something" // <= empty/omitted property that is required
};

let validateBar = Bar;
if (!validateBar(barPass))
    console.log("ERRORS 1:", validateBar.errors); //Never reaches this because valid

if (!validateBar(barFail))
    console.log("ERRORS 2:", validateBar.errors); //Errors array gets logged
