// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 28, flaming
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let conventionUUID = randomUUID();
let flamingLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();

//Main entity
////examples
let flamingLogExample = {
    id:flamingLogUUID,
    attributes: {
        name: "flaming log",
        status:"done",
    }
};
let flamingLogError = {
    id:flamingLogUUID,
    attributes: {
        name: "flaming log",
        status:"pending",
    }
};

////overlay
let flamingLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'flaming',
    validExamples: [flamingLogExample],
    erroredExamples: [flamingLogError]    
});
//LOOK review description
flamingLog.setMainDescription("Flaming logs are for termination or weed control and allow for single dates or multiple dates based on start, end and frequency.");

flamingLog.setConstant({
    attribute:"status",
    value:"done"
});

//log category 
////examples
let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "weed_control"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        label: "seeding"
    }
};

////overlays
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "weed_control",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});
logCategory.setMainDescription("The log category should be set to weed control. Use this to organize your logs into categories for easier searching and filtering later.");

//convention
let flamingConvention = new builder.ConventionSchema({
    title: "Flaming log",
    version: "0.0.1",
    schemaName:"log--activity--flaming",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"Flaming logs can be created for termination or weed control.",
    // we still have no examples, we will add them later
    validExamples: [],
    erroredExamples: []
});

//add attributes
flamingConvention.addAttribute( { schemaOverlayObject:flamingLog, attributeName: "flaming_log", required: true } );
flamingConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );
flamingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
flamingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "area_unit", required: false});
flamingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

//relationships
flamingConvention.addRelationship( { containerEntity:"flaming_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
flamingConvention.addRelationship( { containerEntity:"flaming_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
flamingConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );
flamingConvention.addRelationship( { containerEntity:"flaming_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );

let plantAssetExampleAttributes = flamingConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = flamingConvention.overlays.plant_asset.validExamples[0].id;

//Convention examples
let flamingConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    flaming_log: {
        id: flamingLogUUID,
        attributes: {
            name: "example flaming log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    }
};

// Rewrite the example
let flamingConventionError = {
    id: conventionUUID,
    type: "object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    flaming_log: {
        id: flamingLogUUID,
        attributes: {
            name: "example flaming log",
            status: "pending",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    /*area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },*/
};


// Now that we have proper examples, let's feed them into our convention object.
flamingConvention.validExamples = [flamingConventionExample];
flamingConvention.erroredExamples = [flamingConventionError];

let secondTest = flamingConvention.testExamples();
let storageOperation = flamingConvention.store();
