// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let seedingLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let conventionUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let seedingRateUnitUUID = randomUUID();
let bedWidthUnitUUID = randomUUID();

//Examples
let seedingLogExample = {
    id:seedingLogUUID,
    attributes: {
        name: "example div veg seeding log",
        status:"done",
        is_movement:"false"
    }
};
let seedingLogError = {
    id:seedingLogUUID,
    attributes: {
        name: "example div veg seeding log",
        status:"true",
        is_movement:"true"
    }
};

let widthUnitExample = {
    id: bedWidthUnitUUID,
    attributes: {
        name: "feet"
    }
};
let widthUnitError = {
    id: bedWidthUnitUUID,
    attributes: {
        name: "kg"
    }
};

let bedWidthExample = {
    attributes: {
        measure:"length/depth",
        label: "bed width"
    }
};

let rateUnitExample = {
    id: seedingRateUnitUUID,
    attributes: {
        name: "kg/hec"
    }
};
let rateUnitError = {
    id: seedingRateUnitUUID,
    attributes: {
        name: "kg/m^2"
    }
};

let areaPercentageExample = {
    id: areaPercentageUUID,
    attributes: {
        label: "area",
        measure: "area"
    }
};

let seedingRateExample = {
    attributes: {
        measure:"rate",
        label: "seeding rate"
    }
};

let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "seeding"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        name: "activity"
    }
};


// // Main entity: Seeding Log
let seedingLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--seeding',
    name: 'seeding',
    validExamples: [seedingLogExample],
    erroredExamples: [seedingLogError]    
});

seedingLog.setMainDescription("A seeding or transplanting log sets the location of the plant asset.");

// required attribute is status
seedingLog.setConstant({
    attribute:"status",
    value:"done"
});

// required to set location of asset
seedingLog.setConstant({
    attribute:"is_movement",
    value:"false"
});

//taxonomy term - log_category
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "seeding",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});
logCategory.setMainDescription("The log category is seeding. Use this to organize your logs into categories for easier searching and filtering later.");

//// Quantities
//area overlay
let areaPercentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "area_percentage",
    validExamples: [areaPercentageExample]
    //erroredExamples: [areaPercentageError]
});
areaPercentage.setMainDescription("Area is the percentage of the field that the seeding applies to.");
areaPercentage.setConstant({
    attribute: "measure",
    value:"area"
});
areaPercentage.setConstant({
    attribute: "label",
    value:"area"
});

// area unit example
let unitExample = {
    id: percentageUnitUUID,
    name: "%"
};
//taxonomy term - units
let unitPercentage = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "%"
});
unitPercentage.setMainDescription("Units for area are: % of acres.");

// seeding rate quantity
let seedingRate = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "seeding_rate",
    validExamples: [ seedingRateExample ]
});
seedingRate.setMainDescription("The seeding rate indicates the mass of seed used for every surface unit.");
seedingRate.setConstant({
    attribute: "measure",
    value:"rate"
});
seedingRate.setConstant({
    attribute: "label",
    value:"seeding rate"
});

// seeding rate unit
let rateUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "rate",
    validExamples: [ rateUnitExample ],
    erroredExamples: [ rateUnitError ]
});
rateUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs/ac",
        "seeds/acre",
        "kg/hec",
        "seeds/hec",
        "plants per sq ft",
        "plants per sq m"
    ],
    description: "Several units are available, both imperial and metric. They are all compatible mass to area ratios."
});
rateUnit.setMainDescription("Unit used to measure the rate quantity.");

// bed width
let bedWidth = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "bed_width",
    validExamples: [ bedWidthExample ]
});
bedWidth.setMainDescription("The bed width indicates the size of permanent beds.");
bedWidth.setConstant({
    attribute: "measure",
    value:"length/depth"
});
bedWidth.setConstant({
    attribute: "label",
    value:"bed width"
});

// bed width unit
let widthUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "feet",
    validExamples: [ widthUnitExample ],
    erroredExamples: [ widthUnitError ]
});
widthUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "feet",
        "meters"
    ],
    description: "Several units are available, both imperial and metric."
});
widthUnit.setMainDescription("Unit used to measure the bed width.");


// Convention
let seedingConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Seeding",
    version: "0.0.1",
    schemaName:"log--seeding--div_veg_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"Seeding log required for every planting asset.",
    validExamples: [],
    erroredExamples: []
});


seedingConvention.addAttribute( { schemaOverlayObject:seedingLog, attributeName: "seeding_log", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:areaPercentage, attributeName: "area_quantity", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:unitPercentage, attributeName: "area_unit", required: true } );
// NEW We can add a relationship to an existing overlay using it's name, like here. In this case, we checked in collection/overlays and saw a folder called `asset--plant--planting`. We can call that overlay using that exact name. This is exactly equivalent to using an overlay we've just written in this file. Below (line 342), I will add a relationship to it.
seedingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: false } );
seedingConvention.addAttribute( {schemaOverlayObject:seedingRate, attributeName:"seeding_rate_quantity", required: false} );
seedingConvention.addAttribute( {schemaOverlayObject:rateUnit, attributeName:"rate_unit", required: false} );
seedingConvention.addAttribute( {schemaOverlayObject:bedWidth, attributeName:"bed_width_quantity", required: false} );
seedingConvention.addAttribute( {schemaOverlayObject:widthUnit, attributeName:"width_unit", required: false} );

seedingConvention.addRelationship( { containerEntity:"seeding_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
seedingConvention.addRelationship( { containerEntity:"seeding_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: true } );
seedingConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: true } );
seedingConvention.addRelationship( { containerEntity:"seeding_rate_quantity" , relationName:"units" , mentionedEntity:"rate_unit" , required: true } );
seedingConvention.addRelationship( { containerEntity:"bed_width_quantity" , relationName:"units" , mentionedEntity:"width_unit" , required: true } );
// Let's write the relationship between our main entity for this convention (the seeding log) and the asset--plant--planting overlay.
seedingConvention.addRelationship(
    {
        // The containerEntity is the main entity, the "seeding_log"
        containerEntity:"seeding_log" ,
        // Looking into the schema for the log--seeding and it's "relationship" fields, we can relate the land asset under the field "location" (LOOK shouldn't this be asset?)
        relationName:"asset",
        // The mentioned entity is added using it's attribute name, as when invokint addAttribute. We've effectively used "addAttribute", so it is the same as if we had just created the overlay.
        // We used `attributeName: "plant_asset"`
        mentionedEntity:"plant_asset",
        required: true
    } );

var seedingConventionExample = {
   id: conventionUUID,
   type: "Object",
   seeding_log: {
    id: seedingLogUUID,
    attributes: {
        name: "example div veg seeding log",
        status: "done",
        is_movement: "false"
    },
    relationships: {
        category: { data: [
            {
            type: "taxonomy_term--log_category",
            id:logCategoryUUID
            }
        ] },
        quantity: { data: [
            {
            type: "quantity--standard",
            id: areaPercentageUUID
            }
        ] }
    },
   },
   log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
},
    area_unit: unitExample
};

var seedingConventionError = {
    id: conventionUUID,
    seeding_log: {
     id: seedingLogUUID,
     attributes: {
         name: "example div veg seeding log",
         status: "done",
         is_movement: false
     },
     relationships: {
         category: { data: [
             {
             type: "taxonomy_term--log_category",
             id:logCategoryUUID
             }
         ] }
     },
    },
    log_category: logCategoryError,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    }
};

seedingConvention.validExamples = [seedingConventionExample];
seedingConvention.erroredExamples = [seedingConventionError];

let test = seedingConvention.testExamples();
let storageOperation = seedingConvention.store();
