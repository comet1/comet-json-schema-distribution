// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 19, soil disturbance
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//ids for the examples
let logUUID = randomUUID();
let stirUUID = randomUUID();
let depthUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let logCategoryUUID = randomUUID();
let tillageLogUUID = randomUUID();
let implementUUID = randomUUID();
let implementUnitUUID = randomUUID();
let depthUnitUUID = randomUUID();
let speedUUID = randomUUID();
let speedUnitUUID = randomUUID();

// main entity examples -- rewritten in next section
// let validExample = { attributes:{ name: "tillage", status:"done", data: "plow" } };
// let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity" } };
// let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong" } };

//main entity
////Example
let validExample = {
    id:tillageLogUUID,
    attributes: {
        name: "tillage",
        status:"done",
        data:"plow",
    }
};
let errorExample_1 = {
    id:tillageLogUUID,
    attributes: {
        name: "tillage_error",
        status:"false",
        data:"activity",
    }
};
let errorExample_2 = {
    id:tillageLogUUID,
    attributes: {
        name: "tillage_error",
        status:"2",
        data:"wrong",
    }
};

let tillageLog = new builder.SchemaOverlay({
    typeAndBundle: "log--activity",
    name: "tillage",
    validExamples: [validExample],
    erroredExamples: [ errorExample_1, errorExample_2 ]
});

tillageLog.setMainDescription("Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*. Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth. May have other taxonomy_term--log_category. Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)");

//stir quantity overlay
let stirQuantity = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "STIR"
});
stirQuantity.setMainDescription("Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.");
stirQuantity.setConstant( {
    attribute:"measure",
    value:"ratio",
} );

//depth quantity overlay
let depthQuantity = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "tillage_depth"
});
depthQuantity.setMainDescription("Must be labelled as *depth* and it's measure type is *length*.");
depthQuantity.setConstant( {
    attribute:"label",
    value:"depth",
} );
depthQuantity.setConstant( {
    attribute:"measure",
    value:"length",
} );

//LOOK do we need a depth unit if it's always inches?
let depthUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "depth_unit"
});
depthUnit.setMainDescription("");

depthUnit.setConstant( {
    attribute: "name",
    value: "in"
})

//speed overlay
let speed = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "tillage_speed"
});
speed.setMainDescription("Must be labelled as *speed* and it's measure type is *speed*.");
speed.setConstant( {
    attribute:"label",
    value:"speed",
} );
speed.setConstant( {
    attribute:"measure",
    value:"speed",
} );

//LOOK do we need a speed unit if it's always mph?
let speedUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "speed_unit"
});
speedUnit.setMainDescription("The speed for mechanized tillage should always be entered in mph");

speedUnit.setConstant( {
    attribute: "name",
    value: "mph"
});


//tillage implement
////Examples
/*let implementExample = {
    id:implementUUID,
    attributes: {
        name: "lbs_acres",
    }
};
let implementError = {
    id:implementUnitUUID,
    attributes: {
        name: "ft",
    }
};
////Overlays and constants
let implement = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "lime unit",
    validExamples: [ limeUnitExample ],
    erroredExamples: [ limeUnitError ]
});
implement.setEnum({
    attribute: "name",
    valuesArray: [
        "add"
    ],
    description: ""
});*/

let tillageLogCat = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name:"tillage"
});

tillageLogCat.setMainDescription("The only identifier for a tillage log activity *MUST* be its tillage log category taxonomy term.");
tillageLogCat.setConstant( {
    attribute:"name",
    value:"tillage"
} );

//tillage convention
let tillageConvention = new builder.ConventionSchema({
    title: "tillage_event",
    version:"0.0.1",
    schemaName:"log--activity--tillage",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"A tillage log encompasses all information we can gather about a tillage operation.",
    validExamples: [  ],
    erroredExamples: [  ]
});

tillageConvention.addAttribute( { schemaOverlayObject:tillageLog, attributeName: "tillage_log", required: true } );
tillageConvention.addAttribute( { schemaOverlayObject:stirQuantity, attributeName: "stir_quantity", required: false } );
tillageConvention.addAttribute( { schemaOverlayObject:depthQuantity, attributeName: "depth_quantity", required: false } );
tillageConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
tillageConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "area_unit", required: false});
tillageConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
tillageConvention.addAttribute({ schemaOverlayObject:tillageLogCat, attributeName: "log_category", required: false});
tillageConvention.addAttribute({ schemaOverlayObject:depthUnit, attributeName: "depth_unit", required: false});
tillageConvention.addAttribute({ schemaOverlayObject:speed, attributeName: "speed", required: false});
tillageConvention.addAttribute({ schemaOverlayObject:speedUnit, attributeName: "speed_unit", required: false});

tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false } );
tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"depth_quantity" , required: false } );
tillageConvention.addRelationship( { containerEntity:"depth_quantity" , relationName:"units" , mentionedEntity:"depth_unit" , required: false } );
tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
tillageConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );
tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"speed" , required: false } );
tillageConvention.addRelationship( { containerEntity:"speed" , relationName:"units" , mentionedEntity:"speed_unit" , required: false } );
tillageConvention.addRelationship( { containerEntity:"tillage_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
tillageConvention.addRelationship( { containerEntity:"tillage_log", relationName:"category", mentionedEntity:"log_category", required: true } );

let plantAssetExampleAttributes = tillageConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = tillageConvention.overlays.plant_asset.validExamples[0].id;

let tillageConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    tillage_log: {
        id: logUUID,
        attributes: {
            status:'done',
            name: "tillage"
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: stirUUID
                },
                {
                    type: "quantity--standard",
                    id: depthUUID
                },
                {
                    type: "quantity--standard",
                    id: speedUUID
                },
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] },
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
        },
    },
    stir_quantity: {
        attributes: {label: "STIR"},
        id: stirUUID
    },
    depth_quantity: {
        attributes: {
            label:"depth"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: depthUnitUUID
                }
            ] }
        },
        id: depthUUID
    },
    depth_unit:  {
        id: depthUnitUUID,
        attributes: {
            name: "in"
        },   
    },
    speed: {
        attributes: {
            label:"speed"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: speedUnitUUID
                }
            ] }
        },
        id: speedUUID
    },
    speed_unit:  {
        id: speedUnitUUID,
        attributes: {
            name: "mph"
        },   
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    log_category:  {
        id: logCategoryUUID,
        attributes: {
            name: "tillage"
        },   
    }
};

let tillageConventionError = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    activity_log: {
        id: logUUID,
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: { data: [ {
                type: "quantity--standard",
                id: '695e1d38-a1d6-4ed7-8d9d-c0b818a2cbfe'
            } ] }
        },
        relationships: {
            quantity: { data: [ {
                type: "quantity--standard",
                id: 'b71f7bb3-da43-40b1-8dc3-965e5f912763'
            } ] }
        }
    },
    stir_quantity: {
        id: logUUID,
        attributes: {label: "stir"}
    },
};

tillageConvention.validExamples = [tillageConventionExample];
tillageConvention.erroredExamples = [tillageConventionError];

let test = tillageConvention.testExamples();
let storageOperation = tillageConvention.store();