// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let seedTreatmentLogUUID = randomUUID();
let conventionUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let areaPercentageUUID = randomUUID();

//Examples
let seedTreatmentLogExample = {
    id: seedTreatmentLogUUID,
    attributes: {
        status: "done"
    }
};

let seedTreatmentLogError = {
    id: seedTreatmentLogUUID,
    attributes: {
        status: "in process"
    }
};

//Main entity
let seedTreatmentLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'seed_treatment',
    validExamples: [seedTreatmentLogExample],
    erroredExamples: [seedTreatmentLogError]
});
seedTreatmentLog.setMainDescription("This adds a seed treatment as an input log if one is noted in planting information.");

seedTreatmentLog.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

//quantity - material (LOOK I don't see a quantity in the survey?)

//Convention
// Object
let seedTreatmentConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Seed Treatment",
    version: "0.0.1",
    schemaName:"log--input--seed_treatment",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"Seed treatment log to be applied to a plant asset only if the farmer selected one in their planting records.",
    // if you want to define this later, don't add it when first creating the object
    // validExamples: [seedTreatmentConventionExample],
    // erroredExamples: [seedTreatmentConventionError]
});

////add attributes
seedTreatmentConvention.addAttribute( { schemaOverlayObject:seedTreatmentLog, attributeName: "seed_treatment_log", required: true } );
seedTreatmentConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--log_category--seeding", attributeName: "log_category", required: true});
seedTreatmentConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

////add relationships
seedTreatmentConvention.addRelationship({ containerEntity: "seed_treatment_log", relationName:"category", mentionedEntity:"log_category", required: true});
seedTreatmentConvention.addRelationship( { containerEntity:"seed_treatment_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );


let plantAssetExampleAttributes = seedTreatmentConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = seedTreatmentConvention.overlays.plant_asset.validExamples[0].id;

let logCategoryExample = seedTreatmentConvention.overlays.log_category.validExamples[0];
let logCategoryUUID = seedTreatmentConvention.overlays.log_category.validExamples[0].id;

var seedTreatmentConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    seed_treatment_log: {
        id: seedTreatmentLogUUID,
        attributes: {
            name: "example seed treatment log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,

    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};

var seedTreatmentConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    seed_treatment_log: {
        id: seedTreatmentLogUUID,
        attributes: {
            name: "example seed treatment log",
            status: "false",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    // LOOK - emily, do I delete this? Didn't want to just in case but I don't see quantity on this one in the relationships
    // area_quantity: {
    //    id: areaPercentageUUID,
    //    attributes: {
    //        label: "area"
    //    },
    //    relationships: {
    //        units: { data: [
    //            {
    //                type:"taxonomy_term--unit",
    //                id: percentageUnitUUID
    //           }
    //        ] }
    //    }
    // },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};

seedTreatmentConvention.validExamples = [seedTreatmentConventionExample];
seedTreatmentConvention.erroredExamples = [seedTreatmentConventionError];

let test = seedTreatmentConvention.testExamples();
let storageOperation = seedTreatmentConvention.store();
