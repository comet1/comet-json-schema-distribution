// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 25, mowing
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let mowingLogUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();

//Main entity
////Examples
let mowingLogExample = {
    id:mowingLogUUID,
    attributes: {
        name: "example mowing log",
        status:"done",
    }
};
let mowingLogError = {
    id:mowingLogUUID,
    attributes: {
        name: "example mowing log",
        status:"pending",
    }
};
////Overlays and constants
let mowingLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'mowing',
    validExamples: [mowingLogExample],
    erroredExamples: [mowingLogError]    
});
mowingLog.setMainDescription("This log includes any type of mowing, including mowing grass (for example, between rows in an orchard), mowing hay (in preparation for bailing), mowing for termination, etc.")

mowingLog.setConstant({
    attribute:"status",
    value:"done"
});

//convention
let mowingConvention = new builder.ConventionSchema({
    title: "Mowing log",
    version: "0.0.1",
    schemaName:"log--activity--mowing",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"Mowing logs can be created for termination or weed control.",
    validExamples: [],
    erroredExamples: []
});

//add attributes
mowingConvention.addAttribute( { schemaOverlayObject:mowingLog, attributeName: "mowing_log", required: true } );
mowingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
mowingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "area_unit", required: false});
mowingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

//add relationships
mowingConvention.addRelationship( { containerEntity:"mowing_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
mowingConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );
mowingConvention.addRelationship( { containerEntity:"mowing_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );

let plantAssetExampleAttributes = mowingConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = mowingConvention.overlays.plant_asset.validExamples[0].id;

//examples
let mowingConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    mowing_log: {
        id: mowingLogUUID,
        attributes: {
            name: "example mowing log",
            status: "done",
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] }
        },
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    }
};
let mowingConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    /*mowing_log: {
        id: mowingLogUUID,
        attributes: {
            name: "example mowing log",
            status: "done",
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] }
        },
    },*/
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    }
};

mowingConvention.validExamples = [mowingConventionExample];
mowingConvention.erroredExamples = [mowingConventionError];

let test = mowingConvention.testExamples();
let storageOperation = mowingConvention.store();