// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 34, NPK fertilizer
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//LOOK can we just use this for nonNPK as well? if we make quantities not required I think it would be the same

// ids for the examples
let NPKfertilizerUUID = randomUUID();
let moisturePercentageUUID = randomUUID();
let nitrogenPercentageUUID = randomUUID();
let phosphorusPercentageUUID = randomUUID();
let potassiumPercentageUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let npkFertilizerLogUUID = randomUUID();
let fertilizerUnitUUID = randomUUID();

//examples
let NPKfertilizerLogExample = {
    id:NPKfertilizerUUID,
    attributes: {
        name: "example NPK fertilizer log",
        status:"done",
    }
};
let NPKfertilizerLogError = {
    id:NPKfertilizerUUID,
    attributes: {
        name: "example NPK fertilizer log",
        status:"true",
    }
};

//Main entity
let NPKfertilizerLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'npk_fertilizer',
    validExamples: [NPKfertilizerLogExample],
    erroredExamples: [NPKfertilizerLogError]    
});

//LOOK add description
NPKfertilizerLog.setMainDescription("This log records nitrogen, phosphorus and potassium fertilizers. It allows the user to create new and/or select multiple fertilizer names and brands. There is another survey question for non-NPK fertilizers. This log has the option to record frequency of application, brand, application method, rate, units, %N, %P, %K and other minerals.")

NPKfertilizerLog.setConstant({
    attribute:"status",
    value:"done"
});

//Fertilizer quantity units
////Examples
let fertilizerUnitExample = {
    id:fertilizerUnitUUID,
    attributes: {
        name: "lbs_acres",
    }
};
let fertilizerUnitError = {
    id:fertilizerUnitUUID,
    attributes: {
        name: "ft",
    }
};
////Overlays and constants
let fertilizerUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "fertilizer unit",
    validExamples: [ fertilizerUnitExample ],
    erroredExamples: [ fertilizerUnitError ]
});
fertilizerUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "cubic_ft",
        "cubic_yard",
        "gallons_acre",
        "g_acre",
        "g_bedft",
        "g_rowft",
        "g_sqft",
        "inch",
        "kg_acre",
        "kg_ha",
        "kg_rowmeter",
        "kg_sqm",
        "lbs_acre",
        "lbs_bedft",
        "lbs_rowft",
        "lbs_sqft",
        "liters_acre",
        "metric_tons_acre",
        "metric_tons_ha",
        "oz_acre",
        "oz_bedft",
        "oz_rowft",
        "oz_sqft",
        "gallons",
        "g",
        "kg",
        "lbs",
        "liters",
        "metric_tons",
        "oz",
        "us_tons",
        "us_tons_acre"
    ],
    description: "List of fertilizer units for both liquid and solid amendments."
});

//convention
let NPKfertilizerConvention = new builder.ConventionSchema({
    title: "NPK Fertilizer Input",
    version: "0.0.1",
    schemaName:"log--input--npk_fertilizer",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK add description
    description:"This log records nitrogen, phosphorus and potassium fertilizers.",
    validExamples: [NPKfertilizerConventionExample],
    erroredExamples: [NPKfertilizerConventionError]
});

//LOOK do we need method or brand (don't think so because there's nothing additional that we need beyond what's already specified in shcema). Debugger saying schema doesn't exist for quantity--standard--moisture_percentage
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:NPKfertilizerLog, attributeName: "npk_fertilizer_log", required: true } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "percentage_unit", required: false});
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--moisture_percentage", attributeName: "moisture", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--nitrogen_percentage", attributeName: "nitrogen", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--phosphorus_percentage", attributeName: "phosphorus", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--potassium_percentage", attributeName: "potassium", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--material--rate", attributeName: "rate", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:fertilizerUnit, attributeName: "rate_unit", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--amendment", attributeName: "log_category", required: false } );


//add relationships
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"quantity" , mentionedEntity:"moisture" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"moisture" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"quantity" , mentionedEntity:"nitrogen" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"nitrogen" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"quantity" , mentionedEntity:"phosphorus" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"phosphorus" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"quantity" , mentionedEntity:"potassium" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"potassium" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"quantity" , mentionedEntity:"rate" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"npk_fertilizer_log" , relationName:"category" , mentionedEntity:"log_category" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"rate" , relationName:"units" , mentionedEntity:"rate_unit" , required: false } );

//examples
let plantAssetExampleAttributes = NPKfertilizerConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = NPKfertilizerConvention.overlays.plant_asset.validExamples[0].id;

let rateExampleAttributes = NPKfertilizerConvention.overlays.rate.validExamples[0].attributes;
let rateUUID = NPKfertilizerConvention.overlays.rate.validExamples[0].id;

let logCategoryExampleAttributes = NPKfertilizerConvention.overlays.log_category.validExamples[0].attributes;
let logCategoryUUID = NPKfertilizerConvention.overlays.log_category.validExamples[0].id;

var NPKfertilizerConventionExample = {
    id: NPKfertilizerUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    npk_fertilizer_log: {
        id: npkFertilizerLogUUID,
        attributes: {
            name: "example NPK fertilizer log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id: logCategoryUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: moisturePercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: nitrogenPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: phosphorusPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: potassiumPercentageUUID
                },
                {
                    type: "quantity--material",
                    id: rateUUID
                }
                ]} 
        },
    },
    log_category: {
        id: logCategoryUUID,
        attributes: logCategoryExampleAttributes
    },
    rate: {
        id: rateUUID,
        attributes: rateExampleAttributes,
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: fertilizerUnitUUID
                }
            ] }
        }
    },
    rate_unit:{
        id: fertilizerUnitUUID,
        attributes: {
            name: "g_acre"
        }
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    nitrogen: {
        id: nitrogenPercentageUUID,
        attributes: {
            label: "nitrogen",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    phosphorus: {
        id: phosphorusPercentageUUID,
        attributes: {
            label: "phosphorus",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    potassium: {
        id: potassiumPercentageUUID,
        attributes: {
            label: "potassium",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    }
};

var NPKfertilizerConventionError = {
    id: NPKfertilizerUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    npk_fertilizer_log: {
        id: npkFertilizerLogUUID,
        attributes: {
            name: "example NPK fertilizer log",
            status: "pending",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: moisturePercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: nitrogenPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: phosphorusPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: potassiumPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: rateUUID
                }
                ]} 
        },
    },
    rate: {
        id: rateUUID,
        attributes: rateExampleAttributes
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    nitrogen: {
        id: nitrogenPercentageUUID,
        attributes: {
            label: "nitrogen",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    phosphorus: {
        id: phosphorusPercentageUUID,
        attributes: {
            label: "phosphorus",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    potassium: {
        id: potassiumPercentageUUID,
        attributes: {
            label: "potassium",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    }
};

NPKfertilizerConvention.validExamples = [NPKfertilizerConventionExample];
NPKfertilizerConvention.erroredExamples = [NPKfertilizerConventionError];

let test = NPKfertilizerConvention.testExamples();
let storageOperation = NPKfertilizerConvention.store();