// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 41, grazing
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let logCategoryUUID = randomUUID();
let grazingLogUUID = randomUUID();
let animalCountUUID = randomUUID();
let animalWeightUUID = randomUUID();
let subpaddocksUUID = randomUUID();
let conventionUUID = randomUUID();
let weightUnitUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();

//Main entity
////Examples
let grazingLogExample = {
    id:grazingLogUUID,
    attributes: {
        name: "grazing log",
        status:"done",
    }
};
let grazingLogError = {
    id:grazingLogUUID,
    attributes: {
        name: "grazing log",
        status:"pending",
    }
};
////overlays and constants
let grazingLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'grazing',
    validExamples: [grazingLogExample],
    erroredExamples: [grazingLogError]    
});
//LOOK review description
grazingLog.setMainDescription("Grazing logs are used to indicate when a herd was moved in and/or out of an area. This log records field, % of area covered, type of livestock, grazing type, animal count, average weight, and grazing purpose");
grazingLog.setConstant({
    attribute:"status",
    value:"done"
});

//Log Category
////Examples
let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "grazing"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        label: "seeding"
    }
};
////overlays and constants
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "grazing",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});
//LOOK set description
logCategory.setMainDescription("Use this to organize your logs into categories for easier searching and filtering later.");

//quantity - weight
////Examples
let animalWeightExample = {
    id: animalWeightUUID,
    attributes: {
        measure: "weight"
    }
};
let animalWeightError = {
    id: animalWeightUUID,
    attributes: {
        measure: "count"
    }
};
////overlays and constants
let animalWeight = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "animal_weight",
    validExamples: [ animalWeightExample ],
    erroredExamples: [ animalWeightError ]
});
//LOOK set description
animalWeight.setMainDescription("Animal weight is an average weight of the herd involved in the grazing event. Options are available in kg or lb.");
animalWeight.setConstant({
    attribute: "measure",
    value:"weight"
});

//Weight unit
////Examples
let weightUnitExample = {
    id: weightUnitUUID,
    attributes: {
        name: "lbs"
    }
};
////overlays and constants
let weightUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "weight_unit",
    validExamples: [ weightUnitExample ],
    //erroredExamples: [ weightUnitError ]
});
//LOOK from taxonomy list in surveystack beta management question 22, list needs to be updated
weightUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs"
    ],
    //LOOK set description
    description: "Units used to indicate average weight of animals grazing. "
});

//quantity - count LOOK do we need units for these quantities? they are just numbers so I would assume not
//Animal Count
////Examples
let animalCountExample = {
    id: animalCountUUID,
    attributes: {
        measure: "count"
    }
};
let animalCountError = {
    id: animalCountUUID,
    attributes: {
        measure: "weight"
    }
};
////overlays and constants
let animalCount = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "animal_count",
    validExamples: [ animalCountExample ],
    erroredExamples: [animalCountError]
});
//LOOK set description
animalCount.setMainDescription("Animal count refers to the number of animals involved in this grazing event.");
animalCount.setConstant({
    attribute: "measure",
    value:"count"
});

//quantity - sub-paddocks
////examples
let subpaddocksExample = {
    id: subpaddocksUUID,
    attributes: {
        measure: "count"
    }
};
let subpaddocksError = {
    id: subpaddocksUUID,
    attributes: {
        measure: "weight"
    }
};
let subpaddocks = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "subpaddocks",
    validExamples: [ subpaddocksExample ],
    erroredExamples: [ subpaddocksError ]
});
//LOOK set description
subpaddocks.setMainDescription("...");
subpaddocks.setConstant({
    attribute: "measure",
    value:"count"
});

//convention
let grazingConvention = new builder.ConventionSchema({
    title: "Grazing log",
    version: "0.0.1",
    schemaName:"log--activity--grazing",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK set description
    description:"Grazing logs can be created for termination or for general herd managment.",
    validExamples: [],
    erroredExamples: []
});

//add attributes
grazingConvention.addAttribute( { schemaOverlayObject:grazingLog, attributeName: "grazing_log", required: true } );
grazingConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );
grazingConvention.addAttribute( { schemaOverlayObject:animalCount, attributeName: "animal_count", required: false } );
grazingConvention.addAttribute( { schemaOverlayObject:animalWeight, attributeName: "animal_weight", required: false } );
grazingConvention.addAttribute( { schemaOverlayObject:weightUnit, attributeName: "weight_unit", required: false } );
grazingConvention.addAttribute( { schemaOverlayObject:subpaddocks, attributeName: "subpaddocks", required: false } );
grazingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
grazingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "area_unit", required: false});
grazingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

//relationships
grazingConvention.addRelationship( { containerEntity:"grazing_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
grazingConvention.addRelationship( { containerEntity:"grazing_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
grazingConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );
grazingConvention.addRelationship( { containerEntity:"grazing_log" , relationName:"quantity" , mentionedEntity:"animal_count" , required: false } );
grazingConvention.addRelationship( { containerEntity:"grazing_log" , relationName:"quantity" , mentionedEntity:"animal_weight" , required: false } );
grazingConvention.addRelationship( { containerEntity:"grazing_log" , relationName:"quantity" , mentionedEntity:"subpaddocks" , required: false } );
grazingConvention.addRelationship( { containerEntity:"grazing_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
grazingConvention.addRelationship( { containerEntity:"animal_weight" , relationName:"units" , mentionedEntity:"weight_unit" , required: false } );

let plantAssetExampleAttributes = grazingConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = grazingConvention.overlays.plant_asset.validExamples[0].id;

// Rewrite the example
let grazingConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    grazing_log: {
        id: grazingLogUUID,
        attributes: {
            name: "example grazing log",
            status: "done",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: animalCountUUID
                },
                {
                    type: "quantity--standard",
                    id: animalWeightUUID
                },
                {
                    type: "quantity--standard",
                    id: subpaddocksUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    animal_count: {
        id: animalCountUUID,
        attributes: {
            label: "head"
        },
        relationships: { }
    },
    subpaddocks: {
        id: subpaddocksUUID,
        attributes: {
            label: "subsections"
        },
        relationships: { }
    },
    animal_weight: {
        id: animalWeightUUID,
        attributes: {
            label: "weight"
        },
        relationships: { 
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: weightUnitUUID
                }
            ] }
        }
    },
    weight_unit:  {
        id: weightUnitUUID,
        attributes: {
            name: "lbs"
        },   
    },
};

// Errored convention example
let grazingConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    grazing_log: {
        id: grazingLogUUID,
        attributes: {
            name: "example grazing log",
            status: "done",
        },
        relationships: {
/*            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }, */
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: animalCountUUID
                },
                {
                    type: "quantity--standard",
                    id: animalWeightUUID
                },
                {
                    type: "quantity--standard",
                    id: subpaddocksUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    animal_count: {
        id: animalCountUUID,
        attributes: {
            label: "head"
        },
        relationships: { }
    },
    subpaddocks: {
        id: subpaddocksUUID,
        attributes: {
            label: "subsections"
        },
        relationships: { }
    },
    animal_weight: {
        id: animalWeightUUID,
        attributes: {
            label: "weight"
        },
        relationships: { 
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: weightUnitUUID
                }
            ] }
        }
    },
    weight_unit:  {
        id: weightUnitUUID,
        attributes: {
            type: "taxonomy_term--unit",
            name: "lbs"
        },   
    },
};

grazingConvention.validExamples = [grazingConventionExample];
grazingConvention.erroredExamples = [grazingConventionError];

let test = grazingConvention.testExamples();
let storageOperation = grazingConvention.store();