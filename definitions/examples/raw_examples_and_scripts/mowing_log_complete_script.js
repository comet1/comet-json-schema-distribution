const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let mowingConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--activity--mowing/object.json` ) );
let mowingConvention = new builder.ConventionSchema( mowingConventionSrc );

// reading the example
let mowingExample = exampleImporter(filename = "mowingLogSurvey_complete.json");

Object.keys(mowingConvention.schema.properties);

// empty object
let formattedExample = {
    mowing_log: {},
    plant_asset: {},
    area_quantity:{},
    area_unit:{}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = mowingExample[0];

//second entity is the area unit (%)
formattedExample.area_unit = mowingExample[1];

//third entity is the mowing log
formattedExample.mowing_log = mowingExample[2];

//fourth entity is the plant asset
formattedExample.plant_asset = mowingExample[3];

// testing the example
mowingConvention.validExamples = [ formattedExample ];

let test = mowingConvention.testExamples();