const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let tillageConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--activity--tillage/object.json` ) );
let tillageConvention = new builder.ConventionSchema( tillageConventionSrc );

// reading the example
let tillageExample = exampleImporter(filename = "tillageLogSurvey_mechanized_basic.json");

Object.keys(tillageConvention.schema.properties);

// empty object
let formattedExample = {
    tillage_log: {},
    stir_quantity: {},
    depth_quantity:{},
    area_quantity:{},
    area_unit: {},
    plant_asset: {},
    log_category: {}
};

//the 1st entity in the convention is the quantity--standard, area
formattedExample.area_quantity = tillageExample[0];

//the 2nd entity in the convention is the taxonomy_term--unit, %
formattedExample.area_quantity = tillageExample[1];

//the 3rd entity in the convention is quantity--standard, stir
formattedExample.stir_quantity = tillageExample[2];

//the 4th entity in the convention is log--activity
formattedExample.tillage_log = tillageExample[3];

//the 5th entity in the convention is asset--plant
formattedExample.plant_asset = tillageExample[4];

//the 6th entity in the convention is taxonomy_term--log_category
formattedExample.log_category = tillageExample[5];

//7th entity is also log category

//8th entity is asset--equipment

tillageConvention.validExamples = [ formattedExample ];

let test = tillageConvention.testExamples();