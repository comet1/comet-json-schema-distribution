const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let limeConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--input--lime/object.json` ) );
let limeConvention = new builder.ConventionSchema( limeConventionSrc );

// reading the example
let limeExample = exampleImporter(filename = "limeLogSurvey_complete.json");

// empty object
let formattedExample = {
    lime_log: {},
    lime_quantity:{},
    lime_unit:{},
    plant_asset: {},
    area_quantity: {},
    percentage_unit:{},
    log_category: {},
    lime_type: {}
};

//the first entity in the convention is the lime quantity, assigning that here
formattedExample.lime_quantity = limeExample[0];

//second entity is the taxonomy_term--material_type
formattedExample.lime_unit = limeExample[1];

//third entity is the taxonomy_term--material_type
formattedExample.lime_type = limeExample[2];

//fourth entity is also taxonomy_term--material_type

//fifth entity is an input log
formattedExample.lime_log = limeExample[4];

//sixth entity is plant asset
formattedExample.plant_asset = limeExample[5];

//seventh entity is taxonomy term log category
formattedExample.log_category = limeExample[6];

// testing the example
limeConvention.validExamples = [ formattedExample ];

let test = limeConvention.testExamples();