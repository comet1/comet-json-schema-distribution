const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let organicMatterConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--input--organic_matter/object.json` ) );
let organicMatterConvention = new builder.ConventionSchema( organicMatterConventionSrc );

// reading the example
let organicMatterExample = exampleImporter(filename = "organicMatterInputLogSurvey_complete.json");

Object.keys(organicMatterConvention.schema.properties);

// empty object
let formattedExample = {
    organic_matter_log: {},
    log_category: {},
    plant_asset:{},
    area_quantity:{},
    percentage_unit: {},
    moisture: {},
    nitrogen:{},
    phosphorus: {},
    potassium:{},
    rate:{},
    organic_matter_type: {},
    organic_matter_unit: {}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = organicMatterExample[0];

//2nd entity is the area unit (%)
formattedExample.area_unit = organicMatterExample[1];

//3rd entity is the rate
formattedExample.rate = organicMatterExample[2];

//4th entity is unit (lbs)
formattedExample.organic_matter_unit = organicMatterExample[3];

//5th entity is material_type
formattedExample.organic_matter_type = organicMatterExample[4];

//6th entity is quantity--standard (moisture)
formattedExample.moisture = organicMatterExample[5];

//7th entity is % again

//8th entity is quantity--standard (n)
formattedExample.nitrogen = organicMatterExample[7];

//9th entity is % again

//10th entity is quantity--standard (p)
formattedExample.phosphorus = organicMatterExample[9];

//11th entity is % again

//12th entity is quantity--standard (k)
formattedExample.potassium = organicMatterExample[11];

//13th entity is % again

//14th entity is log--input
formattedExample.organic_matter_log = organicMatterExample[13];

//15th entity is asset--plant
formattedExample.plant_asset = organicMatterExample[14];

//16th entity is taxonomy_term--log_category
formattedExample.log_category = organicMatterExample[15];

// testing the example
organicMatterConvention.validExamples = [ formattedExample ];

let test = organicMatterConvention.testExamples();