const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let irrigationConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--input--irrigation/object.json` ) );
let irrigationConvention = new builder.ConventionSchema( irrigationConventionSrc );

// reading the example
let irrigationExample = exampleImporter(filename = "irrigationLogSurvey_basic.json");

Object.keys(irrigationConvention.schema.properties);

// empty object
let formattedExample = {
    irrigation_log: {},
    plant_asset: {},
    area_quantity:{},
    percentage_unit:{},
    log_category: {},
    total_water:{},
    water_unit: {},
    effectiveness:{},
    effectiveness_unit:{}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = irrigationExample[0];

//2nd entity is the area unit (%)
formattedExample.area_unit = irrigationExample[1];

//3rd entity is the irrigation log
formattedExample.irrigation_log = irrigationExample[2];

//4th entity is plant asset
formattedExample.plant_asset = irrigationExample[3];

//5th entity is log category
formattedExample.log_category = irrigationExample[4];

//entities are repeated again

// testing the example
irrigationConvention.validExamples = [ formattedExample ];

let test = irrigationConvention.testExamples();