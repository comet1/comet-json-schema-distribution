const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let irrigationConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--input--irrigation/object.json` ) );
let irrigationConvention = new builder.ConventionSchema( irrigationConventionSrc );

// reading the example
let irrigationExample = exampleImporter(filename = "irrigationLogSurvey_complete.json");

Object.keys(irrigationConvention.schema.properties);

// empty object
let formattedExample = {
    irrigation_log: {},
    plant_asset: {},
    area_quantity:{},
    percentage_unit:{},
    log_category: {},
    total_water:{},
    water_unit: {},
    effectiveness:{},
    effectiveness_unit:{}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = irrigationExample[0];

//2nd entity is the area unit (%)
formattedExample.area_unit = irrigationExample[1];

//3rd entity is quantity--material 
formattedExample.total_water = irrigationExample[2];

//4th entity is quantity--material 
formattedExample.water_unit = irrigationExample[3];

//5th entity is taxonomy_term--material_type (well) this needs to be added to convention

//6th entity is log--input 
formattedExample.irrigation_log = irrigationExample[5];

//7th entity is asset--plant 
formattedExample.plant_asset = irrigationExample[6];

//8th entity is log--category
formattedExample.log_category = irrigationExample[7];

//entities repeat themselves

// testing the example
irrigationConvention.validExamples = [ formattedExample ];

let test = irrigationConvention.testExamples();