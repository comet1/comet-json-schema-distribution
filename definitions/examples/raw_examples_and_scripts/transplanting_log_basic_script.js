//this seems like the basic and complete would be the same
const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let transplantingConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--transplanting--div_veg_planting/object.json` ) );
let transplantingConvention = new builder.ConventionSchema( transplantingConventionSrc );

// reading the example
let transplantingExample = exampleImporter(filename = "transplantingLogSurvey_basic.json");

Object.keys(transplantingConvention.schema.properties);

// empty object
let formattedExample = {
    transplanting_log: {},
    log_category: {},
    area_quantity:{},
    area_unit:{},
    plant_asset:{}
};

//the first entity in the convention is the plant asset
formattedExample.plant_asset = transplantingExample[0];

//2nd taxonomy term plant type is missing

//3rd taxonomy term season is missing

//4th log seeding is missing

//fifth entity is log category
formattedExample.log_category = transplantingExample[4];