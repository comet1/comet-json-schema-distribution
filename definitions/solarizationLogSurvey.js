// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 30, solarization
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let solarizationLogUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let logCategoryUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let solarizationQuantityUUID = randomUUID();

//Main entity
let solarizationLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'solarization',
    validExamples: [solarizationLogExample],
    erroredExamples: [solarizationLogError]    
});
//LOOK review description
solarizationLog.setMainDescription("This log is used to record solairzation events, records % of field covered.")

solarizationLog.setConstant({
    attribute:"status",
    value:"done"
});

let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "weed_control",
    validExamples: [logCategoryExample],
    //erroredExamples: [logCategoryError],
});
logCategory.setMainDescription("The log category should be set to weed control.");

//Convention
let solarizationConvention = new builder.ConventionSchema({
    title: "Solarization log",
    version: "0.0.1",
    schemaName:"log--activity--solarization",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK check description
    description:"Solarization events used for weed control.",
    validExamples: [solarizationConventionExample],
    erroredExamples: [solarizationConventionError]
});

//add attributes
solarizationConvention.addAttribute( { schemaOverlayObject:solarizationLog, attributeName: "solarization_log", required: true } );
solarizationConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--weed_control", attributeName: "log_category", required: true } );
solarizationConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
solarizationConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "area_unit", required: false});
solarizationConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

//relationships
solarizationConvention.addRelationship( { containerEntity:"solarization_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
solarizationConvention.addRelationship( { containerEntity:"solarization_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
solarizationConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );
solarizationConvention.addRelationship( { containerEntity:"solarization_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );

//examples
var solarizationLogExample = {
    id: solarizationLogUUID,
    attributes: {
        status: "done"
    }
};

var solarizationLogError = {
    id: solarizationLogUUID,
    attributes: {
        status: "processing"
    }
};

var logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "weed_control"
    }
}

let plantAssetExampleAttributes = solarizationConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = solarizationConvention.overlays.plant_asset.validExamples[0].id;

var solarizationConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    solarization_log: {
        id: solarizationLogUUID,
        attributes: {
            name: "example solarization log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },

    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};

var solarizationConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    solarization_log: {
        id: solarizationLogUUID,
        attributes: {
            name: "example solarization log",
            status: "false",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--material",
                    id: solarizationQuantityUUID
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};


solarizationConvention.validExamples = [solarizationConventionExample];
solarizationConvention.erroredExamples = [solarizationConventionError];

let test = solarizationConvention.testExamples();
let storageOperation = solarizationConvention.store();