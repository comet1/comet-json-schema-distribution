const {apiComposeTaxonomiesTransformation, exampleImporter, fixRelationshipDataField, findMissingEntitiesInExample} = require("../src/schema_utilities.js");
const fs = require("fs");
const { randomUUID } = require('crypto');

describe( "Fixing basic schemas", () => {
    test("'data' field, removed by FarmOS.js, is properly added back to all relationships while storinng example schemas.", () => {

        let schema = {
            type: "object",
            properties: {
                relationships: {
                    type:"object",
                    properties: { "location": {
                        "type": "array",
                        "title": "Location",
                        "items": {
                            "type": "object",
                            "required": [
                                "id",
                                "type"
                            ],
                            "properties": {
                                "id": {
                                    "type": "string",
                                    "title": "Resource ID",
                                    "format": "uuid",
                                    "maxLength": 128
                                },
                                "type": {
                                    "type": "string"
                                }
                            }
                        }
                    } }
                }
            }
        }
        ;
        fixRelationshipDataField(schema);
        expect(schema.properties.relationships.properties.location.type).toBe('object');
        expect(schema.properties.relationships.properties.location.properties.data.type).toBe('array');
    });
} );

describe("Example importing functionality.", () => {

    test("An entity with implied taxonomies gets decoupled into many schema compliant entities, and these are correctly mentioned into the relationships.",  () => {

        // diverse vegetatle api compose
        const apiComposeExample = JSON.parse( fs.readFileSync(`${__dirname}/dataExamples/api_compose_data_example.json`) );
        let transformationOutput = apiComposeTaxonomiesTransformation( apiComposeExample.find( d => d.entity.type == "log--seeding" ).entity );
        let createdTaxonomyTermLogCat = transformationOutput.newEntities.find( d => d.type == "taxonomy_term--log_category" );
        // new entities have the right structure in and outside "attributes"
        expect( Object.keys( createdTaxonomyTermLogCat ) ).toStrictEqual(["id", "type", "attributes"]);
        expect( Object.keys( createdTaxonomyTermLogCat.attributes ) ).toStrictEqual(["name"]);
        // the relationship now has the proper structure
        expect( Object.keys( transformationOutput.mainEntity.relationships.category.data[0] ) ).toStrictEqual( ["type","id"] );
        // the entity correctly establishes the relationship
        expect( transformationOutput.mainEntity.relationships.category.data[0].id ).toBe( createdTaxonomyTermLogCat.id );
        expect( transformationOutput.mainEntity.relationships.category.data[0].type ).toBe( "taxonomy_term--log_category" );
    });

    test("An entity with two implied taxonomies gets two newEntities",  () => {

        // diverse vegetable api compose
        const apiComposeExample = JSON.parse( fs.readFileSync(`${__dirname}/dataExamples/api_compose_data_example.json`) );
        let transformationOutput = apiComposeTaxonomiesTransformation( apiComposeExample.find( d => d.entity.type == "asset--plant" ).entity );

        expect(transformationOutput.newEntities.length).toBe(2);
        });

    test("Dates are properly reparsed when using examples importer.",  () => {
        // diverse vegetable api compose
        let example = exampleImporter( "api_compose_data_example.json", basePath = `${__dirname}/dataExamples` );
        let expectedTimestamp = '2023-05-18T04:00:00.000Z';
        let readTimestamp = example.find( entity => entity.type == "log--seeding" ).attributes.timestamp;

        expect(readTimestamp).toBe(expectedTimestamp);
    });

    test("Main user facing function for this feature, exampleImporter, is able to retrieve a file and process it.", () => {
        let example = exampleImporter( "api_compose_data_example.json", basePath = `${__dirname}/dataExamples` );
        expect(example.filter( d => d.type.includes("taxonomy")).length ).toBe(6);
    });

    test("Missing examples detector works", () => {
        let example = [
            {
                type: 'quantity--standard',
                id: 'c5daa14a-6e08-47ce-9935-86252e5260a3',
                attributes: { value: { value: { decimal: 1 }, label: 'active_ingredient_percent' }, label: 'active_ingredient_percent' },
                relationships: { units: {
                    data: [
                        {
                            type: 'taxonomy_term--unit',
                            id: '4ed95592-0ec8-47f5-9fe8-998567d79957'
                        }
                    ]
} }
            },
            {
                id: '4ed95592-0ec8-47f5-9fe8-998567d79957',
                type: 'taxonomy_term--unit',
                attributes: { name: '%' }
            },
            {
                type: 'log--input',
                attributes: {
                    name: 'insecticide 2 4 d 1 cubic ft',
                    status: 'done',
                    notes: [Object],
                    is_termination: true,
                    timestamp: '2023-05-31T04:00:00.000Z'
                },
                relationships: {
                    asset: {
                        data: [
                            {
                                type: 'asset--plant',
                                id: 'cba27c69-9a31-40b7-8f40-3ef690fdb9c6'
                            }
                        ]
},
                    category: {
                        data: [
                            {
                                type: 'taxonomy_term--log_category',
                                id: 'fef5183c-9f6e-4708-b980-87fa81e659d8'
                            },
                            {
                                type: 'taxonomy_term--log_category',
                                id: 'ec4c4741-d080-45d0-ad79-728171614882'
                            }
                        ]
},
                    quantity: {
                        data: [
                            {
                                type: 'quantity--standard',
                                id: '4eb67df7-6903-4423-ad66-5f406f6cf71d'
                            },
                            {
                                type: 'quantity--material',
                                id: 'c709a558-8ad7-452c-ad01-8e56db34571f'
                            },
                            {
                                type: 'quantity--standard',
                                id: 'c5daa14a-6e08-47ce-9935-86252e5260a3'
                            }
                        ]
}
                },
                id: '246f2455-3462-450b-8b2f-aa3ffed86c39'
            },
        ];
        let search = findMissingEntitiesInExample(example);
        let includedOrMissing = [ ... search.included , ... search.mentionedNotIncluded.map( d => d.id ) ];
        let onlyInMentioned = includedOrMissing.filter( d => !search.mentioned.includes(d) );
        let onlyInIncludedOrMissing = search.mentioned.filter( d => !includedOrMissing.includes(d) );

        expect( search.mentioned.includes(... includedOrMissing) ).toBe( true );
        expect( search.mentioned.length ).toBeLessThanOrEqual( search.included.length + search.mentionedNotIncluded.length );
    });
});
